
var $_GET = (function() {
	var url = window.document.location.href.toString();
	var u = url.split("?");

	if (typeof (u[1]) == "string") {
		u = u[1].split("&");
		var get = {};

		for (var i in u) {
			var j = u[i].split("=");
			get[j[0]] = j[1];
		}

		return get;
	} else {
		return {};
	}
})();

function obj(id,foodid,foodname,foodprice,foodpic){
				this.id = id;
				this.food_id = foodid;
				this.food_name = foodname;
				this.food_price =foodprice;
				this.food_pic = foodpic;
				
			};
//正则传输页面后面的参数；
//用法：GetQueryString("参数名1")；

function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');

    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
//检测本地文件存在与否
function fileExists(URL) {  
    var http = new XMLHttpRequest();  
    http.open('HEAD', URL, false);  
    http.send();
    return http.status;  
} 
		



function setCookie(name, value, iDay) 
{
    var oDate=new Date();
     
    oDate.setDate(oDate.getDate()+iDay);
     
    document.cookie=name+'='+encodeURIComponent(value)+';expires='+oDate;
}
 
function getCookie(name)
{
    var arr=document.cookie.split('; ');
    var i=0;
    for(i=0;i<arr.length;i++)
    {
        //arr2->['username', 'abc']
        var arr2=arr[i].split('=');
         
        if(arr2[0]==name)
        {  
            var getC = decodeURIComponent(arr2[1]);
            return getC;
        }
    }
     
    return '';
}
 
function removeCookie(name){
    setCookie(name, '1', -1);
}



function isPC() {
        var userAgentInfo = navigator.userAgent;
        var Agents = ["Android", "iPhone",
                    "SymbianOS", "Windows Phone",
                    "iPad", "iPod"];
        var flag = true;
        for (var v = 0; v < Agents.length; v++) {
            if (userAgentInfo.indexOf(Agents[v]) > 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }
	
function isTizen(){
	var userAgentInfo = navigator.userAgent;
	var flag = false;
	if (userAgentInfo.indexOf("Tizen") > 0) {
		flag = true;
	}	
	return flag;
}


function isWeiXin(){ 
        var ua = navigator.userAgent.toLowerCase(); 
        if(ua.indexOf('micromessenger') != -1) { 
            return true; 
        } else { 
            return false; 
        } 
    }
	
/**
 * GET请求数据方法 封装的AJAX，适用于Butler,ISTV，门锁等APP.
 * @param {string} Url
 * @param {string} KeyName
 * @returns {unresolved}
 */
function get_data(Url, KeyName) {	
    var defer = $.Deferred();
    $.ajax({
        type: "get",
        url: Url,
        dataType: "json",
		beforeSend: function () {
			// $(".apple ul").html("loading..."); //开始提交之前,显示正在加载层
			// $(".content .contentText").html("loading...");
		},
        success: function (json) {
			// $(".apple ul").html("success...");
			// $(".content .contentText").html("success...");
			defer.resolve(json); 
            if (json['ret'] === 200) { 				
                if (KeyName) localStorage.setItem(KeyName, JSON.stringify(json));
            } else {
                if(KeyName) localStorage.removeItem(KeyName);
            }
        },
		complete: function () {
			// $(".apple ul").html("finished..."); //完成后,隐藏加载层
			// $(".content .contentText").html("finished...");
		},
		error: function (err) {
			// $(".apple ul").html("error...")		
			// $(".content .contentText").html("error...");	
			defer.reject(err);
			if(KeyName) localStorage.removeItem(KeyName);
			
		}
		
    });
    return defer.promise();
}

/**
 * POST请求数据方法 封装的AJAX，适用于Butler,ISTV，门锁等APP.
 * @param {json} Datas
 * @param {string} Url
 * @param {string} KeyName
 * @returns {unresolved}
 */
function post_data(Url,Datas,KeyName) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: Url,
        data: Datas,
        dataType: "json",
        traditional: true,
        contentType: "application/x-www-form-urlencoded",
		beforeSend: function () {
			$("#loading").show(); //开始提交之前,显示正在加载层
		},
        success: function (json) {
            defer.resolve(json);          
            if (json['ret'] === 200) {               
                if (KeyName) localStorage.setItem(KeyName, JSON.stringify(json));
            } else {               
                if(KeyName) localStorage.removeItem(KeyName);
            }
        },
		complete: function () {
			$("#loading").hide(); //完成后,隐藏加载层
		},
		error: function (data) {			
			defer.reject(json);
			if(KeyName) localStorage.removeItem(KeyName);
			console.info("error: " + data.responseText);
		}
		
    });
    return defer.promise();
}




/**
 * 获取页面数据封装函数
 * @param {Object} pageId
 * @param {Object} pageType
 * @param {Object} callback
 */
function getData(pageId, pageType, callback) {
	callback = callback || function() {};
	try {
		
		switch(pageType) {
			
			case "special":
				var url =  localStorage.apiServer + "page/" + pageId + "/";
				break;
				
			case "batch":
				var url =  localStorage.apiServer + "page/batch/?page_id=" + pageId;
				break;

			case "imageBatch":
				var url =  localStorage.apiServer + "image/batch/?image_id=" + pageId;
				break;

			case "image":
				var url =  localStorage.apiServer + "image/" + pageId + "/";
				break;
				
			case "info_page":
				var url =  localStorage.apiServer + "page/search/?page_type=info_page&lang_agnostic_id=" + pageId;
				break;
			case "info_list":
				var url =  localStorage.apiServer + "page/search/?page_type=info_list&lang_agnostic_id=" + pageId;
				break;
				
			case "merchandise":
				var url =  localStorage.apiServer + "page/search/?page_type=merchandise&lang_agnostic_id=" + pageId;
				break;                              
                                
			case "rooms":
				
				delete BASEDATA.device_id;	
				var TIMESTAMP  = parseInt(timestamp/1000);
				var STR =  "/api/junctioncore/v1/hotel/rooms/?hotel_id=0086000009&device_type=tv&timestamp="+TIMESTAMP;
				var url =  localStorage.apiServer + "hotel/rooms/";
				BASEDATA.timestamp = TIMESTAMP;
				BASEDATA.sign = md5(STR,BASEDATA.hotel_id);			
				
				break;
			
			case "times":
				var url =  localStorage.apiServer + "/util/now/";
				break;      	
			default:
				var url =  localStorage.apiServer + "/page/" + pageId + "/";
				break;
		}
		$.ajax({
			type: "GET",
			url: url,
			data: BASEDATA,
			async: true,
			success: function(json) {
//			console.log(url + "\n\r" +JSON.stringify(json));
				try {
					if(json.ret == 200) {	
						
						callback(json.data);								
					} else {
						callback(json);
					}
				} catch(e) {
						callback(e);
				}

			},
			err: function(err) {
				console.log(err);
				if(err.msg == "Invalid device_id."){
					console.log("无效的设备号")
				}
				callback(err);
				
			}
		});
	} catch(errs) {

		//TODO 连接接口发生异常报错
		console.log(errs);
		callback(errs)
	}
}


/**
 * postdata
 */


function postData(datas, action, callback) {
	callback = callback || function() {};
	try {
		
		//对接第三方接口时使用的函数
        var url =   localStorage.apiServer +  "util/proxy/?url=/api/TV/"+action; 
		
		
			methot = "POST";
			companytype = null;
			if(action == 'SubmitOrder' || action=='AddToShoppingCart'){
				companytype = "yingkang";
			}
			url = url + "&hotel_id="+BASEDATA.hotel_id+"&restaurant_id="+BASEDATA.restaurant_id+"&device_id="+BASEDATA.device_id+"&device_type="+BASEDATA.device_type;

		if(action == "register"){
			methot = "POST";
			companytype = null;
			 var url =  localStorage.apiServer + "device/register/";
			 url = url + "?hotel_id="+BASEDATA.hotel_id+"&restaurant_id="+BASEDATA.restaurant_id+"&device_id="+BASEDATA.device_id+"&device_type="+BASEDATA.device_type;
			 
		}
		
			$.ajax({
			type: methot,
			url: url,
			data: datas,
			async: true,
			company: companytype,
			success: function(json) {

				try {
			
					callback(json);

				} catch(e) {
				
					callback(e);
				}

			},
			err: function(err) {
				callback(err);
			}
		});
	} catch(errs) {
		//TODO 连接接口发生异常报错
		callback(errs)
	}
}



//替换所有的回车换行  
function BR1(content)  
{  
	var string = content;
	console.log(string.split('#')[1]);
    try{  
        string=string.replace(/\r\n/g,"<BR>")  
        string=string.replace(/\n/g,"<BR>");  
    }catch(e) {  
        alert(e.message);  
    }  
    return string;  
}  

//替换所有的回车换行  
function BR(content)  
{  
	var string = content; 
    return string.split('#');  
}  


/**
 * 动态加载CSS
 * @param {string} url 样式地址
 * loadCss('http://www.yimo.link/static/css/style.css')
 */
function loadCss(url) {
	var head = document.getElementsByTagName('head')[0];
	var link = document.createElement('link');
	link.type='text/css';
	link.rel = 'stylesheet';
	link.href = url;
	head.appendChild(link);
}


/**
 * @desc 是否是 Undefined 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isUndefined(obj) {
    return obj === void 0;
}
/**
 * @desc 是否是 Null 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isNull(obj) {
    return obj === null;
}
/**
 * @desc 是否是 Boolean 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isBoolean(obj) {
    return typeof(obj) === 'boolean';
}
/**
 * @desc 是否是 Number 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isNumber(obj) {
    return typeof(obj) === 'number';
}
/**
 * @desc 是否是 String 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isString(obj) {
    return typeof(obj) === 'string';
}
/**
 * @desc 是否是 Object 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isObject(obj) {
    return Object.prototype.toString.call(obj) === '[object Object]';
}
/**
 * @desc 是否是 Array 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isArray(obj){
    return Array.isArray?Array.isArray(obj):Object.prototype.toString.call(obj) === '[object Array]';
}
/**
 * @desc 是否是 Function 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isFunction(obj){
    return typeof(obj) === 'function';
}
/**
 * @desc 是否是 Date 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isDate(obj){
    return Object.prototype.toString.call(obj) === '[object Date]';
}
/**
 * @desc 是否是 RegExp 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isRegExp(obj){
    return Object.prototype.toString.call(obj) === '[object RegExp]';
}
/**
 * @desc 是否是 Error 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isError(obj){
    return Object.prototype.toString.call(obj) === '[object Error]';
}
/**
 * @desc 是否是 Arguments 类型检测
 * @param obj 待检测的数据
 * @return {Boolean} 布尔值
 */
function isArguments(obj){
    return Object.prototype.toString.call(obj) === '[object Arguments]';
}

/**

	 * 获取所有可用的网络接口。这些网络接口可以通过Wi-Fi，蜂窝或有线（以太网）网络。

	 */

	function getMac1() {

		var mac = "";

			webapis.network.getAvailableNetworks(function(data) {

				for(var i = 0; i < data.length; i++) {

					if(data[i].ip !== 0) {

						mac = data[i].mac;

					}

				}

			}, function(errs) {

				// mac = JSON.stringify(errs)
				return false;

			});

		return mac;

	}

	function getMac(){
		var mac = '';
		var isSupport = tizen.systeminfo.getCapability("http://tizen.org/feature/network.ethernet");
		if (isSupport==true) {
			tizen.systeminfo.getPropertyValue("ETHERNET_NETWORK", (data) => {
				localStorage.macAddress = data.macAddress;
			}, (error) => { });
		}
		return mac;
	}
	getMac();
	function onSuccessCallback(data) {
		// console.log("The wifi load is " + wifi.macAddress);
		localStorage.macAddress=data.macAddress;
		return data.macAddress;
		
	}
  
	function onErrorCallback(error) {
		console.log("An error occurred " + error.message);
	}
	getMac1();
  