var vm=new Vue({
    el:'#main',
    data:{
      A:'-',
      B:'-',
      C:'-',
      pointer:0,
      seen:true,
      numSeen:false,
      timerID:-1,
      timerID1:-1,
      error:'',
      defaultLanguage:localStorage.defaultLanguage,
      imgList:{},
      channelList:[],
      allData:[]
    },
    created() {
        var i, keyCode = {}, supportedKeys;
        supportedKeys = tizen.tvinputdevice.getSupportedKeys();
        for (i = 0; i < supportedKeys.length; i++) {
            // keyCode[supportedKeys[i].name] = supportedKeys[i].code;
          //  $("#xyy").append('|name->'+supportedKeys[i].name+'|code->'+supportedKeys[i].code);
        }
        var _this=this;
        if(localStorage.macAddress1!=undefined){
            $("#xyy").html(' localStorage.macAddress->'+localStorage.macAddress1);
        }
        this.imgList=JSON.parse(localStorage.imgList);
        $.ajax({
            url:localStorage.apiServer+'/page/butler_tv_channels/?'+ $.param(BASEDATA),
            type:'get',
            success:function(data){
                _this.channelList=data.data;
            },
            error:function(){

            }
        });
        this.seen==true?(clearTimeout(this.timerID),this.hiddenUI()):'';
        this.playTV();  
        // this.readParam();
      this.$nextTick(function(){
        document.onkeydown=function(e){
            vm.error=e.keyCode;
            // console.log(e.keyCode);
            switch (e.keyCode){
                case 40:  //pc Down
                    vm.seen==true?(vm.pointer<vm.channelList.length-1?vm.pointer++:'',clearTimeout(vm.timerID)):vm.seen=true;
                    vm.hiddenUI();
                    vm.$refs.list.scrollTop=60*vm.pointer;
                    break;
                case 38://pc up
                    vm.seen==true?(vm.pointer>0?vm.pointer--:'',clearTimeout(vm.timerID)):vm.seen=true;
                    vm.hiddenUI();
                    vm.$refs.list.scrollTop=60*vm.pointer;
                    break;
                case 39://pc right
                    break;
                case 37://pc left
                    break;
                case 29443://tv enter
                case 13://pc enter
                    if(vm.seen==true){
                        vm.changeChannel();
                    }else{
                        vm.seen=true;
                    }
                    clearTimeout(vm.timerID);
                    vm.hiddenUI();
                    setTimeout(() => {
                        vm.$refs.list.scrollTop=60*vm.pointer;
                    }, 300);
                    break;
                case 48://0
                vm.configNums(0);
                vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                vm.hiddenNumUI();
                    break;
                case 49://1
                vm.configNums(1);
                vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                vm.hiddenNumUI();
              
                    break;
                case 50://2
                vm.configNums(2);
                vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                vm.hiddenNumUI();
                    break;
                case 51://3
                vm.configNums(3);
                vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                vm.hiddenNumUI();
                    break;
                case 52://4
                vm.configNums(4);
                vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                vm.hiddenNumUI();
                    break;
                case 53://5
                    vm.configNums(5);
                    vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                    vm.hiddenNumUI();
                        break;
                case 54://6
                        vm.configNums(6);
                        vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                        vm.hiddenNumUI();
                            break;
                case 55://7
                        vm.configNums(7);
                        vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                        vm.hiddenNumUI();
                            break;
                case 56://8
                        vm.configNums(8);
                        vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                        vm.hiddenNumUI();
                            break;
                case 57://9
                        vm.configNums(9);
                        vm.numSeen==true?clearTimeout(vm.timerID1):vm.numSeen=true;
                        vm.hiddenNumUI();
                        
                            break;
                case 10009://tv back
						tizen.tvwindow.hide(function() {
						});
						window.history.go(-1);
                        break;
                case 428://频道-
              //   vm.seen==true?clearTimeout(vm.timerID):vm.seen=true;
                vm.pointer>0?vm.pointer--:'';
                vm.changeChannel();
                vm.hiddenUI();
                    break;
                case 427://频道+
                //vm.seen==true?clearTimeout(vm.timerID):vm.seen=true;
                vm.pointer<vm.channelList.length-1?vm.pointer++:'';
                vm.changeChannel();
                vm.hiddenUI();
                    break;
                case 10072://home
                $("#xyy").html('i am home');
                     break;
                case 10182://tv exist
                tizen.tvwindow.hide(function() {
                });
                window.history.go(-1);
                    break;

            }
         };
      });
    },
    methods:{
          hiddenUI:function(){
              var _this=this;
            this.timerID=setTimeout(function(){
                _this.seen=false;
                _this.confirmPointer();
              },3000);
              return false;
          },
          confirmPointer:function(){
            var channel = tizen.tvchannel.getCurrentChannel();
            this.channelList.forEach((item,index) => {
                if(item.tvChannel.number==channel.major){
                    this.pointer=index;
                    //  $("#xyy").append(' _this.pointer->'+_this.pointer);
                    //  $("#xyy").append('  index->'+index); 
                    //  $("#xyy").append(' major->'+channel.major);
                }
            });
          },
          hiddenNumUI:function(){
            var _this=this;
            var cNum=parseInt(_this.A+''+_this.B+''+_this.C);
            if(!isNaN(cNum)&&cNum<=this.channelList.length)
            {
                this.pointer=cNum;
            }else{
                _this.A=_this.B=_this.C="-";
                return false;
            }
            var major=this.channelList[cNum].tvChannel.number;
            this.timerID1=setTimeout(function(){
                _this.numSeen=false;
                tizen.tvchannel.tune({
                    major: parseInt(major),

                }, {
                    onsuccess:function(channel,b) {
                        
                    },
                    onnosignal:function() {
                    }
                });
                _this.A=_this.B=_this.C="-";
            },3000);
            return false;
          },
          hiddenNumUI1:function(){
            var _this=this;
           
            this.timerID1=setTimeout(function(){
                _this.numSeen=false;
                _this.A=_this.B=_this.C="-";
            },5000);
            return false;
          },
          configNums:function(num){
                isNaN(this.A)?this.A=num:isNaN(this.B)?this.B=num:isNaN(this.C)?this.C=num:(this.A=num,this.B=this.C="-");
          },
          playTV:function(){
            tizen.tvwindow.show(this.successCB, null, ["0px", "0px", "1920px", "1080px"], "MAIN", "BEHIND");
          },
          successCB:function(){
              //    $("#xyy").append('nums->'+nums);
              var _this=this;
              var params = {};
             // params.broadcastStandard = "DVB";
             // params.channelType = "CDTV";
              // params.frequency = 347000;
              //  params.modulationType = "64QAM";
              //  params.bandwidth = "8Mhz";
             // params.symbolRate = 6875; //6900;
             //   params.programNumber = 501; //2;//1027;//2;//200; //service ID
              var major = this.channelList[this.pointer].tvChannel.number;
            //   $("#xyy").html('major->'+major+'|this.pointer->'+this.pointer);
              params.major = parseInt(major);
              tizen.tvchannel.tune(params, {
                onsuccess:function (a,b) {
                    _this.confirmPointer();
                    _this.showCurChannel();
                }, onnosignal:function () {
                }
              });
              return false;
          },
          readParam:function(){
            try {
                // gets 10 channel information among all channels
                tizen.tvchannel.getChannelList(this.successCB1, null, "ALL", 0, 10);
            } catch (error) {
                $("#xyy").append("Error name = " + error.name + ", Error message = " + error.message);
            }
          },
          successCB1:function(channels){
            $("#xyy").append("getChannelList() is successfully done. Totally " + channels.length + " channels are retrieved.");
            for (var i = 0; i < channels.length; i++) {
                /*$("#xyy").append("----- Channel [" + i + "] -----");
                $("#xyy").append("Major channel = " + channels[i].major+'/');
                $("#xyy").append("Minor channel = " + channels[i].minor);
                $("#xyy").append("Channel Name = " + channels[i].channelName);
                $("#xyy").append("Program Number = " + channels[i].programNumber);*/
                // $("#xyy").append('|'+JSON.stringify(channels[i])+'|');
            }
          },
          changeChannel:function(){
              var _this=this;
              var major=this.channelList[this.pointer].tvChannel.number;
            tizen.tvchannel.tune({
                major: parseInt(major),

            }, {
                onsuccess:function(a,b) {
                    _this.showCurChannel();
                    },
                    onnosignal:function() {
                    }
                });
                return false;
          },
          showCurChannel:function(){
            var _this=this;
            var _pointer= _this.pointer+'';
            _pointer.length==1?(_this.A='0',_this.B=_pointer,_this.C=''):_pointer.length==2?(_this.A=_pointer[0],_this.B=_pointer[1],_this.C=''):_pointer.length==3?(_this.A=_pointer[0],_this.B=_pointer[1],_this.C=_pointer[2]):'';
            _this.numSeen=true;
            _this.hiddenNumUI1();
          }
    }
});
for(var i=0;i<=9;i++){
    tizen.tvinputdevice.registerKey(i+'');
}
tizen.tvinputdevice.registerKey("ChannelUp");
tizen.tvinputdevice.registerKey("ChannelDown");
tizen.tvinputdevice.registerKey("Exit");//10182
tizen.tvinputdevice.registerKey("Guide");//457
tizen.tvinputdevice.registerKey("ChannelList");//10073


function getMac1(){
    var isSupport=tizen.systeminfo.getCapability("http://tizen.org/feature/network.ethernet");
    $("#xyy").append('|isSupport->'+isSupport+'|');
    tizen.systeminfo.getPropertyValue("ETHERNET_NETWORK", onSuccessCallback, onErrorCallback);
  }

  function onSuccessCallback(data) {
      // console.log("The wifi load is " + wifi.macAddress);
     // localStorage.macAddress=wifi.macAddress;
    //   $("#xyy").append('|wifi.macAddress->'+wifi.macAddress+'|');
    $("#xyy").append('|data-->'+data+'|');
      $("#xyy").append('|data-->'+JSON.stringify(data)+'|');
      
  }

  function onErrorCallback(error) {
    $("#xyy").append('---error---');
  }
//   getMac1();