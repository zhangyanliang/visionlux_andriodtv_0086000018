if (LANG == "zh-cn") {
	$(".cityname").eq(0).html("北京");
	$(".cityname").eq(1).html("巴黎");
	$(".cityname").eq(2).html("东京");
	$(".cityname").eq(3).html("纽约");
	$(".cityname").eq(4).html("莫斯科");
	$(".cityname").eq(5).html("伦敦");
	$(".zone").html("中国标准时间");
}


//对接世界时间接口;
BASEDATA.city_id = "beijing,paris,tokyo,new-york-ny,moscow,london";
var requestUrl = localStorage.apiServer + "/util/city_time/?" + $.param(BASEDATA);

get_data(requestUrl).then(
	function (json) {
		console.log(json);
		$(".litime").eq(0).append(json.data.beijing);
		$(".litime").eq(1).append(json.data.paris);
		$(".litime").eq(2).append(json.data.tokyo);
		$(".litime").eq(3).append(json.data["new-york-ny"]);
		$(".litime").eq(4).append(json.data.moscow);
		$(".litime").eq(5).append(json.data.london);
	},
	function (err) {
		console.error(err);

	}
)
