/**
 * 获取服务器时间接口
 */

(function(){	
	syncTime();	
})();

function syncTime(){
	getData(BASEDATA,"times",function(jsondata){	
		// var servertimestamp = parseInt(jsondata.timestamp)*1000;
		
		var servertimestamp = Date.parse(new Date((jsondata.datetime).split("-").join("/")));
		console.log("synctime",servertimestamp);
		
		
		localStorage.timestamp = servertimestamp;

		var _time_ = new Date(servertimestamp);	

		console.log("__time__",_time_);
		var week= ['Sun','Mon','Tues','Wed','Thurs','Fri','Sat'];
		var _year_ = _time_.getFullYear();		
		var _month_ = _time_.getMonth() + 1;
		var _date_ = _time_.getDate()
		var _day_ = '日一二三四五六'.charAt(_time_.getDay());
		var _day_en = week[_time_.getDay()];
		var H = _time_.getHours();
		console.log('------>'+typeof H);
		if(new Date().getTimezoneOffset() == 0){
			H = parseInt(H) + 8; //兼容某些设备的时区是0
		}
		var _hours_ = addzero(H);
		var _minutes_ = addzero(_time_.getMinutes());
		var _seconds_ = addzero(_time_.getSeconds());

	
		
		$("#time").text(_hours_ + ":" + _minutes_);
		$("#time2").text(_hours_ + ":" + _minutes_);
		if (localStorage.defaultLanguage == "zh-cn") {
			$("#data").text( _year_ + '年' + _month_ + '月' + _date_ + '日'+' '+'周' + _day_);
		}else{
			$("#data").text( _year_ + '-' + _month_ + '-' + _date_ + ' ' + _day_en);
		}
	
		
		
	});
}

function addzero(n) {
	if(n==undefined){
		n='';//未能立刻获取到返回值,设置空值
	}
	return n > 9 ? n : '0' + n;
}
