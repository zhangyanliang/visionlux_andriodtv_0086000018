
//预置获取全站图片接口(此接口必须预先调用，接下来其它接口返回的数据中如果需要图片，需要从此接口的结果中获取。)

(function () {
	// $(".content .contentText").html(navigator.appVersion);
	var requestUrl = localStorage.apiServer + "/image/?" + $.param(BASEDATA);
	$.ajax(requestUrl).then(
		function (json) {
			
			console.log("good_img:", json);
			localStorage.imgList = JSON.stringify(json);
			getRoom();
			getHotel();
		},

		function (err) {
			console.error("Error_img: " + err.responseText);
			if(isTizen()) location.href="tv.html"; //如果获取图片接口异常,可以认为连接不到junction,直接跳转到播放电视频道.
		}
	);
})();

/**
 * 新junction接口对接
 */

		




//1.设备查询接口,获取房间号
function getRoom(){
	var requestUrl = localStorage.apiServer + "/device/get/?" + $.param(BASEDATA);
	$.ajax(requestUrl).then(
	    function (json) {
	        console.log("getRoom:", json);
			localStorage.roomNum = json.data.device.room;
			
	    },
	
	    function (err) {
	        console.error("getRoom: " + err.responseText);
					if(err.status == 500){
						var json = err.responseJSON;
						if(json.msg == "Device not found.") {
							$(".errorDiv").css("display", "block");
							$(".errorText").html("设备未注册");
							location.href = "register.html";
						} else if(err.statusText == "Internal Server Error") {
							$(".errorDiv").css("display", "block");
							$(".errorText").html("服务器外网中断，系统5秒后进入离线模式");
							setTimeout(function(){
								window.localStorage.roomId = "服务器外网中断";
								
							}, 5000)
						} else {
							$(".errorDiv").css("display", "block");
							$(".errorText").html("服务器发生未知错误，系统5秒后进入离线模式");
							setTimeout(function(){
								window.localStorage.roomId = "服务器发生未知错误";
								
							}, 5000);
						}
					}
					 else {
						$(".errorDiv").css("display", "block");
						$(".errorText").html(err.responseText);
						$(".errorText").append($.param(BASEDATA.DEVICEID));
					}
					
	    }
	);
}

//2.获取酒店根信息(保存多语言信息，默认语言参数)
function getHotel(){
	var requestUrl = localStorage.apiServer + "/page/hotel/?" + $.param(BASEDATA);
	$.ajax(requestUrl).then(
	    function (json) {
			console.log("getHotel:", json);
			
			localStorage.defaultLanguage = json.data.defaultLanguage;
		
	        if (localStorage.defaultLanguage == "zh-cn") {
	            $("#cn").focus();
	            setCookie('focusId', i, 1);
	            $("#h32").html("");
	            $(".sele").html('选择');
	            $(".confirm").html("确认");
	        } else {
	            $("#en").focus();
	            setCookie('focusId', i, 1);
	            $("#h31").html("Dear Valued Guest");
	            $("#h32").html(",");
	            $(".sele").html('Select');
	            $(".confirm").html("Confirm");
			}

			getUserInfo();
			getWelcomePage(json.data.defaultLanguage);
			
			
	    },
	
	    function (err) {
	        console.error("getHotel: " + err.responseText);
	    }
	);
}

//3.如果对接PMS获取设备所在房间顾客信息
function getUserInfo(){
	var requestUrl = localStorage.apiServer + "/hotel/guest_profile/?" + $.param(BASEDATA);
	console.log('++++')
	$.ajax(requestUrl).then(
	    function (json) {
			// console.log("getUserInfo:", json);
			// console.log('defaultLanguage',defaultLanguage)
			var userName = '';
			console.log('******');
	        if (json.ret === 200) {
	            if (json.data.length === 0) {
					
	                 userName = localStorage.defaultLanguage == "zh-cn" ? "贵宾 " : "Valued Guest";     
	               
	
	            }else{//modified  by zhangyanliang
					// json.data.translates[defaultLanguage].username
					userName = json.data[0].last_name+json.data[0].first_name;
				}
				console.log('userName->'+userName);
				var dear = localStorage.defaultLanguage == "zh-cn" ? "尊敬的" : "Dear ";
				var h32 = localStorage.defaultLanguage == "zh-cn" ? "" : ","; 	
	                $("#h31").html(dear + userName);
	                $("#h32").html(h32);
	        }
	    },
	
	    function (err) {
	        console.error("getUserInfo: " + err.responseText);
			console.log('----');
	    }
	);

}

//4:对接餐厅接口
function getRestaurantInfo(){	

	BASEDATA.page_type = 'restaurant';
	BASEDATA.lang_agnostic_id = 'r001'
	var requestUrl = localStorage.apiServer + "/page/search/?" + $.param(BASEDATA);
	console.log('get_restaurant:', requestUrl);
	$.ajax(requestUrl).then(
	    function (json) {
			console.log("getRestaurantInfo:", json);
			
			delete BASEDATA.page_type;
			delete BASEDATA.lang_agnostic_id;
	
	    },
	
	    function (err) {
	        console.error("getRestaurantInfo: " + err.responseText);
	    }
	);

}


//5:对接欢迎页接口
function getWelcomePage(defaultLanguage){
	
	delete BASEDATA.page_type;
	delete BASEDATA.lang_agnostic_id
	var requestUrl = localStorage.apiServer + "/page/butler_welcomepage/?" + $.param(BASEDATA);
	console.log('butler_welcomepage:', requestUrl);
	$.ajax(requestUrl).then(
	    function (json) {
			var IMGLIST = localStorage.imgList ?  JSON.parse(localStorage.imgList) : {};
	    var logo = isUndefined(json.data.welcome.logo) ? "" : IMGLIST.data[json.data.welcome.logo.internalImageId].internal_image_url;
			
			 
		 
			console.log(BR(json.data.translates[localStorage.defaultLanguage].welcomeMessage));
			var string = BR(json.data.translates[localStorage.defaultLanguage].welcomeMessage);
			console.log('defaultLanguage---->' + localStorage.defaultLanguage);
			$(".content .contentText .aa").html(BR1(string[0].trim()));
			$(".content .contentText .bb").html(BR1(string[1].trim()));

			//背景图片,视频,TV频道 三选一
			var backgroudimg = isUndefined(json.data.welcome.backgroundImage) ?  "" : IMGLIST.data[json.data.welcome.backgroundImage.internalImageId].internal_image_url ;
			var backgroundVideoUrl = isUndefined(json.data.welcome.backgroundVideoUrl) ? "" : json.data.welcome.backgroundVideoUrl;
			var channalNo =isUndefined(json.data.welcome.backgroundTvChannel) ? "" : json.data.welcome.backgroundTvChannel;
			if(backgroundVideoUrl != ""){
				try {
					$("body").css("background","transparent");			
						window.JS.setVodUrl(backgroundVideoUrl);					
				} catch (error) {				
					$("body").css("background","transparent");
					// $("body").append('<video src="'+backgroundVideoUrl+'" autoplay="autoplay" loop="loop"></video>');
					var v = document.createElement("video");
					v.setAttribute("src", backgroundVideoUrl);
					v.setAttribute("width",1280);
					v.setAttribute("height",720);
					v.setAttribute("controls", "controls");
					document.body.appendChild(x);
					v.load();
					v.play();
				}				
			}else if(backgroudimg !=""){
				$("body").css("background",'url("'+backgroudimg+'") no-repeat top center');
				$("body").css("background-size",'cover');				
			}else{
				//背景电视频道
			}
			
			//背景音乐
			var backgroundAudio = isUndefined(json.data.welcome.backgroundMusicUrl) ? "" : json.data.welcome.backgroundMusicUrl;
			if(backgroundAudio !=""){			
				localStorage.setItem("backgroundAudio",backgroundAudio);
				if(isTizen()){						   
					webapis.avplay.open(backgroundAudio);
					webapis.avplay.setDisplayRect(0, 0, 1920, 1080);
					webapis.avplay.prepare();
					webapis.avplay.play();
				}else{
					var x = document.createElement("AUDIO");
					x.setAttribute("src", backgroundAudio);
					x.setAttribute("controls", "controls");
					document.body.appendChild(x);
					x.load();
					x.play();
					$(x).hide();
				}
			}
			
			if(json.data.welcome.managerSignature != undefined){		
				var managerSignature = '<img src="' + IMGLIST.data[json.data.welcome.managerSignature.internalImageId].internal_image_url + '" />';
				$(".content .boss").html(managerSignature);
			}
			
	       
			$(".left img").attr("src", logo);

			localStorage.backgroudimg = backgroudimg;
			delete BASEDATA.page_type;
			delete BASEDATA.lang_agnostic_id;
			
			getWeather(defaultLanguage);
	    },
	
	    function (err) {
	        console.error("getWelcomePage: " + err.responseText);
	    }
	);
	
	
}


//6:获取天气预报接口
function getWeather(defaultLanguage){

	BASEDATA.page_type = 'info_page';
	BASEDATA.lang_agnostic_id = 'local_weather';
	BASEDATA.lang = defaultLanguage;
	var requestUrl = localStorage.apiServer + "/page/search/?" + $.param(BASEDATA);
	console.log('getWeather:', requestUrl);
	$.ajax(requestUrl).then(
	    function (json) {
				console.log("getWeather:", json);
				//保存天气预报图标和温度到临时缓存中
				localStorage.weather = JSON.stringify(json);
				sessionStorage.weather_imgurl = 'images/'+BASEDATA.hotel_id+'/wicon/' + json.data.weather_data.today.weatid + ' X42.png';
				sessionStorage.temperature = json.data.weather_data.today.temperature;
				var welcome_weather_imgurl = 'images/'+BASEDATA.hotel_id+'/wicon/' + json.data.weather_data.today.weatid + ' X56.png';
				//向页面渲染数据
				$(".weather span").html(json.data.weather_data.today.temperature);       
				$(".weather img").attr('src',  welcome_weather_imgurl);
				delete BASEDATA.page_type;
				delete BASEDATA.lang_agnostic_id;
				delete BASEDATA.lang;
	    },
	
	    function (err) {
	        console.error("getWeather: " + err.responseText);
	    }
	);
}
