var RUNMODE = "debug";
var HOTELID = "0086000015";
var RESTAURANTID = "r001";
var DEVICEID = "84:62:23:24:25:ED";

if (!isPC()) {
	DEVICEID  = JS.getMac();
}

var BASEDATA = {
	hotel_id:HOTELID,
	device_type:'tv',
	device_id:DEVICEID,
	restaurant_id:RESTAURANTID
}
localStorage.apiServer = (RUNMODE == "debug") ? "https://junction.dev.havensphere.com/api/junctioncore/v1" : "/api/junctioncore/v1";

