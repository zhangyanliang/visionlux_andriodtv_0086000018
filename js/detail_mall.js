/**
 * 获取Junction后台数据接口
 * @param {string} pageId 格式 ‘333，234，344’
 */

function getInfo(pageId){
    BASEDATA.page_id = pageId;
    var requestUrl = localStorage.apiServer + "/page/batch/?" + $.param(BASEDATA);
    get_data(requestUrl).then(function(json){
        var title = '';
        var str = '';

        for (var k = 0; k < json.data.length; k++) {

            /*处理图片逻辑*/
            var imageURLs = [];		
                /*如果存在图片,则遍历对象获取所有图片的ID并保存到图片数组中,否则获取LOGO作为图片*/
                if (json.data[k].hasOwnProperty("info")) {
                    if (json.data[k].info.images.length > 0) {
                        $.each(json.data[k].info.images, function () {
                            imageId = $(this)[0].internalImageId;
                            if (imageId) imageURLs.push(IMGLIST.data[imageId].internal_image_url);
                        
                        });

                    }
                } else {
                    /*如果LOGO不存在,则自动显示404默认图片,否则将LOGO的ID保存到图片数组中*/
                    if (JSON.stringify(json.data[k].logo) === "{}") {
                        console.log("后台没有上传图片自动显示404图片");

                    } else {
                        imageId = json.data[k].logo.internalImageId;
                        if (imageId) imageURLs.push((IMGLIST.data[imageId].internal_image_url));
                        
                    }

                }


            /**
             * 内容框架DOM结构
             */
    

                //左侧标题区
                title += '<li onclick="showContent($(this).index());">' + json.data[k].translates[localStorage.defaultLanguage].title + '</li>';

                //右侧内容区
                str+='<div class="box">';
                str+='<div class="imgtemp flexslider">';
                str+='<ul class="slides">';
                if(imageURLs.length > 0){

                for(var j=0; j < imageURLs.length; j++){

                    str+='<li><div class="img"><img data-imgid="" src="'+imageURLs[j]+'" alt="" /></div></li>';

                }
                

                }

                str+='</ul>';
                str+='</div>';
                str+='<div class="righttextdiv"><ul class="">'+json.data[k].translates[localStorage.defaultLanguage].description+'</div>';
                str+='</ul></div>';

            
        }
        $("#left").html(title);
        $("#content").html(str);
    
				
        $("#left p").eq(0).addClass("cur").siblings().removeClass('cur');
        $("#left li").eq(0).addClass("cur").siblings().removeClass('cur');						
        $("#content .box").eq(0).addClass("cur").siblings().removeClass('cur');
				
				

        $('.imgtemp').flexslider({
            animation: "slide",
            direction:"horizontal",
            easing:"swing"
        });
        $("ol").hide();
        $("ol").attr("display","none");
        $(".flex-direction-nav").attr("display","none");
        $(".flex-direction-nav").hide();	
				
				//标题滚动	
				var Maxlength = 10;
				if(localStorage.defaultLanguage == "en")  Maxlength = 20;				
					$("#left li").remove("marquee");
				if($("#left li").eq(0).html().length > Maxlength){
					$("#left li").eq(0).addClass("marquee").siblings().remove("marquee");
				}
				
				//内容区上下滚动				
                $("#content .righttextdiv ul").removeClass("scroll");
				if($("#content .righttextdiv ul").eq(0).height() > $("#content .righttextdiv").eq(0).height()){
					$("#content .righttextdiv ul").eq(0).addClass('scroll').siblings().removeClass("scroll");
				}
				

				
    },function(err){
        console.error(err);
    });


    
}

function showContent(index){

    $("#left p").eq(index).addClass("cur").siblings().removeClass('cur'); P
		$("#left li").eq(index).addClass("cur").siblings().removeClass('cur');      						
		$("#content .box").eq(index).addClass("cur").siblings().removeClass('cur');
      
    
}
